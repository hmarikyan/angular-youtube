var controllersModule = angular.module('controllersModule',[]);

//var flash_message = "";

/**
 * Index controller
 */
controllersModule.controller('IndexController',['$scope', '$location', 'googleService', function($scope, $location, googleService){
    $scope.google_logged_in = false;

    // check logged in
    googleService.is_logged().then(function(){
        $scope.google_logged_in = true;
    });

    $scope.login = function(){
        googleService.login().then(function(){
            $scope.google_logged_in = true;
            $location.path('/search');
        });
    };
}]);

/**
 * SEARCH VIDEOS
 */
controllersModule.controller('SearchController',['$scope','$location', 'googleService', function($scope, $location, googleService){

    $scope.google_logged_in = false;
    $scope.search_autocomplete_data = null;

    $scope.search_max_results = 15;
    $scope.autoplay = true;
    $scope.play_next = true;

    $scope.records = [];
    $scope.selected_video_id = "";
    $scope.selected_video = null;

    googleService.is_logged().then(function(){
       //console.log("logged");
    },function(){
        $location.path('/');
    });

    $scope.search_term = "ad";

    //search handler
    $scope.search = function(){
        if (!$scope.search_form.$valid)
            return;

        googleService.search_videos($scope.search_term, $scope.search_max_results).then(function(response){
            $scope.selected_video = null;
            $scope.records = response.items;
            //console.log(response);
        })
    };

    $scope.$watch("search_term",function(term){
        if(term){
            googleService.autocomplete(term).then(function(response){
                if(typeof response.data[1] !== 'undefined' && response.data[1].length > 0){
                    $scope.search_autocomplete_data = response.data[1];
                }
                //console.log(response.data);
            })
        }
    });

    //select video
    $scope.select_video = function(index){
        //console.log(video_id);
        $scope.selected_video_id = $scope.records[index].id.videoId;
        $scope.selected_video = index;
    };
    $scope.$on('youtube.player.ready', function ($event, player) {
        // play it again
        if($scope.autoplay == true){
            player.playVideo();
        }
    });
    $scope.$on('youtube.player.ended', function ($event, player) {
        // play the next video
        if($scope.play_next == true) {
            if ($scope.records[$scope.selected_video + 1]) {
                $scope.select_video($scope.selected_video + 1);
            } else {
                $scope.select_video(0);
            }
        }
    });

}]);

/**
 * PLAYLISTS
 */
controllersModule.controller('PlaylistController',['$scope','$location', 'googleService', function($scope, $location, googleService){
    $scope.playlists = [];
    $scope.selected_playlist = null;

    $scope.playlist_items = [];
    $scope.selected_video = null;
    $scope.selected_video_id = null;

    $scope.search_max_results = 15;
    $scope.autoplay = true;
    $scope.play_next = true;

    googleService.is_logged().then(function(){

        //get playlists as soon as login checked
        googleService.get_playlists().then(function(response){
            //console.log(response);
            $scope.playlists = response.items;
        });

    },function(){
        $location.path('/');
    });

    $scope.select_playlist = function(index){
        $scope.selected_playlist = index;

        $scope.selected_video = null;
        $scope.playlist_items = [];

        var playlist_id = $scope.playlists[index].id;
        googleService.get_playlist_items(playlist_id, $scope.search_max_results).then(function(response){
            //console.log(response);
            $scope.playlist_items = response.items;
        });
    };

    //select video
    $scope.select_video = function(index){
        //console.log(video_id);
        $scope.selected_video_id = $scope.playlist_items[index].contentDetails.videoId;
        $scope.selected_video = index;
    };
    $scope.$on('youtube.player.ready', function ($event, player) {
        // play it again
        if($scope.autoplay == true){
            player.playVideo();
        }
    });
    $scope.$on('youtube.player.ended', function ($event, player) {
        // play the next video
        if($scope.play_next == true) {
            if ($scope.playlist_items[$scope.selected_video + 1]) {
                $scope.select_video($scope.selected_video + 1);
            } else {
                $scope.select_video(0);
            }
        }
    });

}]);


/**
 * Upload
 */
controllersModule.controller('UploadController',['$scope','$location','$http', 'googleService', function($scope, $location, $http, googleService){

    $scope.file = null;
    $scope.error_message = null;
    $scope.uploading_progress = null;
    $scope.uploaded = null;
    $scope.video_id = null;

    // google uploader functions
    $scope.callbacks = {
        onError: function(data){
            try {
                var errorResponse = JSON.parse(data);
                message = errorResponse.error.message;
            } finally {
                if(message)
                    $scope.error_message = message;
            }

            $scope.$apply();
        },
        onProgress: function(data){
            //console.log(data);
            $scope.uploaded = null;

            $scope.uploading_progress = {};
            $scope.uploading_progress.status = "Uploading...";
            $scope.uploading_progress.bytesUploaded = data.loaded;
            $scope.uploading_progress.totalBytes = data.total;

            $scope.uploading_progress.percentageComplete = Math.round( ($scope.uploading_progress.bytesUploaded * 100) / $scope.uploading_progress.totalBytes );

            $scope.file = null;

            $scope.$apply();
        },
        onComplete: function(video_id){
            //console.log(video_id);

            $scope.uploaded = true;
            $scope.uploading_progress.status = "Processing...";

            $scope.$apply();
        },
        onPollComplete: function(data){
            //console.log(data);

            var uploadStatus = data.items[0].status.uploadStatus;
            switch (uploadStatus) {
                case 'uploaded':
                    $scope.uploading_progress.status = "Processing...";
                    break;
                case 'processed':
                    $scope.uploading_progress.status = "Done...";

                    $scope.video_id = data.items[0].id;
                    $scope.uploading_progress = null;

                    break;
            }

            $scope.$apply();
        }
    };

    // call from directive
    $scope.file_select = function(file){
        $scope.file = file;
    };

    //logged in
    googleService.is_logged().then(function(){

        //upload click
        $scope.upload = function(){
            googleService.upload_video($scope.file, $scope.callbacks);
        };

    },function(){
        $location.path('/');
    });
}]);
