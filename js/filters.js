var filtersModule = angular.module('filtersModule',[]);

//memory size format
filtersModule.filter('bytes', function() {
    return function(bytes, precision) {
        if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) return '-';

        var unit = "Bytes";

        //kb
        if(bytes > 1024){
            bytes = (bytes/1024).toFixed(2);
            unit = "Kb";
        }
        //mb
        if(bytes > 1024){
            bytes = (bytes/1024).toFixed(2);
            unit = "Mb";
        }

        return bytes + ' ' + unit;
    }
});