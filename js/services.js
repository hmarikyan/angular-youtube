var servicesModule = angular.module('servicesModule',[]);


/**
 * YouTube video uploader class
 *
 * @constructor
 */
servicesModule.factory('youtube_upload',['$window', function($window){
    return function(gapi, access_token, callbacks) {
        var that = this;

        if(typeof callbacks === "undefined"){
            this.callbacks = {
                onError: function(){},
                onProgress: function(){},
                onComplete: function(){},
                onPollComplete: function(){}
            }
        }else{
            this.callbacks = callbacks;
        }

        this.STATUS_POLLING_INTERVAL_MILLIS = 5 * 1000; // One minute.
        this.gapi =  gapi;
        this.access_token = access_token;
        this.uploadStartTime = 0;
        this.videoId = "";

        /**
         * @param file
         */
        this.uploadFile = function(file) {
            var metadata = {
                snippet: {
                    title: file.name,
                    description: file.name,
                    tags: ['youtube-cors-upload'],
                    categoryId: 22
                },
                status: {
                    privacyStatus: "public"
                }
            };

            var uploader = new MediaUploader({
                baseUrl: 'https://www.googleapis.com/upload/youtube/v3/videos',
                file: file.original,
                token: that.access_token,
                metadata: metadata,
                params: {
                    part: Object.keys(metadata).join(',')
                },
                onError: that.callbacks.onError,
                onProgress: that.callbacks.onProgress,
                onComplete: function(data) {
                    var uploadResponse = JSON.parse(data);
                    that.videoId = uploadResponse.id;

                    that.callbacks.onComplete(that.videoId);
                    that.pollForVideoStatus();
                }
            });

            // This won't correspond to the *exact* start of the upload, but it should be close enough.
            that.uploadStartTime = Date.now();
            uploader.upload();
        };

        /**
         *
         */
        this.pollForVideoStatus = function() {
            that.gapi.client.request({
                path: '/youtube/v3/videos',
                params: {
                    part: 'status,player',
                    id: that.videoId
                },
                callback: function(response) {
                    if (response.error) {
                        console.log(response.error.message);
                        $window.setTimeout(that.pollForVideoStatus, that.STATUS_POLLING_INTERVAL_MILLIS);
                    } else {

                        var uploadStatus = response.items[0].status.uploadStatus;

                        //status
                        switch (uploadStatus) {
                            case 'uploaded':
                                that.callbacks.onPollComplete(response);
                                $window.setTimeout(that.pollForVideoStatus, that.STATUS_POLLING_INTERVAL_MILLIS);
                                break;
                            case 'processed':
                                that.callbacks.onPollComplete(response);
                                break;
                            default:
                                break;
                        }
                    }
                }
            });
        };
    };
}]);


/**
 * Fetch google api credentials
 */
servicesModule.service('google_creds',['$http', function($http){
    return $http({
        method: 'GET',
        url: 'config/google_client_id.json'
    })
}]);

servicesModule.service('googleService', ['$http', '$timeout', '$q','$window', 'google_creds', 'youtube_upload', function ($http, $timeout, $q, $window, google_creds, youtube_upload) {
    var that = this;

    this.gapi = null;

    this.google_logged_in = null;

    /**
     * Load google script
     * @returns {*}
     */
    this.lazyLoadApi = function(){
        var deferred = $q.defer();

        function loadScript() {
            // use global document since Angular's $document is weak
            var s = $window.document.createElement('script');
            s.src = 'https://apis.google.com/js/client.js?onload=youtubeLoad';
            $window.document.body.appendChild(s);
        }

        $window.youtubeLoad = function(){
            $window.setTimeout(function(){deferred.resolve(); } , 30);
        };

        if ($window.attachEvent) {
            $window.attachEvent('onload', loadScript);
        } else {
            $window.addEventListener('load', loadScript, false);
        }
        return deferred.promise;
    };

    /**
     * Init library: load library if needed
     * @param creds
     * @returns {*}
     */
    this.init_library = function(){

        var deferred = $q.defer();

        if(that.gapi){
            deferred.resolve();
        }
        else if(typeof gapi !=='undefined' && typeof gapi.client !=='undefined'){
            that.gapi = gapi;
            deferred.resolve();
        }
        else{
            that.lazyLoadApi().then(function(){
                that.gapi = gapi;
                deferred.resolve();
            });
        }

        return deferred.promise;
    };


    /**
     * Google authenticate
     * @param creds
     * @returns {*}
     */
    this.login = function(){
        var deferred = $q.defer();

        that.init_library().then(function(){
            google_creds.then(function successCallback(response) {
                var creds = response.data.web;

                that.gapi.client.setApiKey(creds.api_key);

                that.gapi.auth.authorize({
                    client_id: creds.client_id,
                    scope: creds.scopes,
                    immediate: false,
                }, function(authResult){
                    that.init_youtube(authResult, deferred);
                });

            }, function errorCallback(response) {
                deferred.reject("Error: loading credentials");
                console.log(response);
            });
        });

        return deferred.promise;
    };

    //check google logged status
    this.is_logged = function(){
        var deferred = $q.defer();

        if(that.google_logged_in == true){
            deferred.resolve();
        }else{
            that.init_library().then(function(){
                google_creds.then(function successCallback(response) {

                    var creds = response.data.web;

                    that.gapi.client.setApiKey(creds.api_key);
                    that.gapi.auth.authorize({client_id: creds.client_id, scope: creds.scopes, immediate: true},function(authResult){
                        that.init_youtube(authResult, deferred);
                    });

                }, function errorCallback(response) {
                    deferred.reject("Error: loading credentials");
                    console.log(response);
                });

                //console.log(that.gapi);
            });
        }

        return deferred.promise;
    };


    /**
     * LOAD CLIENT LIBRARY
     */
    this.init_youtube = function(authResult, deferred){
        if (authResult && !authResult.error) {
            that.gapi.client.load('youtube', 'v3', function() {
                deferred.resolve();

                that.google_logged_in = true;
            });
        } else {
            deferred.reject('error: google load');
        }
    };

    /**
     * Search
     * @returns {*}
     */
    this.search_videos = function(term, res_count) {

        var deferred = $q.defer();

        //console.log(gapi.client.youtube);
        var request = that.gapi.client.youtube.search.list({
            part: 'id,snippet',
            q: term,
            maxResults: res_count,
            type: "video"
        });
        request.execute(function (response) {
            deferred.resolve(response);
        });

        return deferred.promise;
    };

    //
    this.autocomplete = function(term) {
        return $http({
            method: 'GET',
            url: 'http://localhost/angular-youtube/php/google_autocomplete.php?term='+ term
        });
    };


    /**
     * GET playlistS
     * @returns {*}
     */
    this.get_playlists = function(){
        var deferred = $q.defer();

        //console.log(gapi.client.youtube);
        var request = that.gapi.client.youtube.playlists.list({
            part: 'contentDetails,id,snippet,player,status',
            mine: true,
            //maxResults: res_count,
        });
        request.execute(function (response) {
            deferred.resolve(response);
        });

        return deferred.promise;
    };

    /**
     * GET PLAYLIST RECORDS/VIDEOS
     * @param playlist_id
     * @returns {*}
     */
    this.get_playlist_items = function(playlist_id, res_count){
        var deferred = $q.defer();

        //console.log(gapi.client.youtube);
        var request = that.gapi.client.youtube.playlistItems.list({
            part: 'contentDetails,id,snippet,status',
            playlistId: playlist_id,
            maxResults: res_count
        });
        request.execute(function (response) {
            deferred.resolve(response);
        });

        return deferred.promise;
    };

    /**
     * UPLOAD
     * @returns {*}
     */
    this.upload_video = function(file, callbacks){
        if(file){
            var response  = that.gapi.auth.getToken();
            var access_token = response.access_token;
            var ob = new youtube_upload(that.gapi, access_token, callbacks);
            ob.uploadFile(file);
        }else{
            console.log("Error: no file passed")
        }
    };

    return this;

}]);