//main module and load dependencies
var youtubeModule = angular.module('youtubeModule', [
    'ngRoute',
    'controllersModule',
    'servicesModule',
    'directivesModule',
    'filtersModule',
    'mgcrea.ngStrap',
    'autocomplete',
    'youtube-embed'
]);

// ROUTING
youtubeModule.config(['$routeProvider', function ($routeProvider) {
        $routeProvider.
            when('/', {
                templateUrl: 'templates/index.html',
                controller: 'IndexController'
            }).
            when('/search', {
                templateUrl: 'templates/search.html',
                controller: 'SearchController'
            }).
            when('/playlist', {
                templateUrl: 'templates/playlist.html',
                controller: 'PlaylistController'
            }).
            when('/upload', {
                templateUrl: 'templates/upload.html',
                controller: 'UploadController'
            }).
            otherwise({
                redirectTo: '/'
            });
    }
]);