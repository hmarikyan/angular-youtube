var directivesModule = angular.module('directivesModule',[]);

//file drag/drop
directivesModule.directive("dropfile", function($parse) {
    return {
        restrict : "A",
        scope: false,
        link: function (scope, elem, attrs) {

            //get scope function name
            var onFiledrop = $parse(attrs.onFiledrop);

            //handle drop
            elem.bind('drop', function(evt) {
                evt.stopPropagation();
                evt.preventDefault();

                //get all files
                var files = evt.dataTransfer.files;
                for (var i = 0, f; f = files[i]; i++) {

                    //read file by file_api
                    var reader = new FileReader();
                    reader.readAsDataURL(f);

                    var newFile = {
                        name : f.name,
                        type : f.type,
                        size : f.size,
                        lastModifiedDate : f.lastModifiedDate,
                        original: f
                    };

                    reader.onload = (function(e) {
                        newFile.content = e.target.result;

                        //send it to scope
                        scope.$apply(function() {
                            onFiledrop(scope, {$file: newFile});
                        });
                    });
                }
            });

            //prevent dragover event
            elem.bind('dragover', function(evt) {
                evt.stopPropagation();
                evt.preventDefault();
                evt.dataTransfer.dropEffect = 'copy';
            });
        }
    }
});

