Indroduction:
=============

This is a test task written in javascript AngularJS
Working with Google youtube server by it's library.

Pages:
=============

   * Login
   
   * Search
   
   * My playlists
   
   * Upload

Requirements:
=============

 * Nodejs
  
 * Web server + php handler
 
 * bower component


Installation:
=============

1. In web server root directory - open console/git bash and execute:
   git clone https://hmarikyan@bitbucket.org/hmarikyan/angular-youtube.git

2. Execute:
   bower install
   
3. Open the browser and type 'http://localhost/angular-youtube' (for standard xampp/wamp servers)